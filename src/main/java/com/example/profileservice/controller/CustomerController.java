protected InterceptorStatusToken beforeInvocation(Object object){
        Assert.notNull(object,"Object was null");
final boolean debug=logger.isDebugEnabled();
        // Here we are checking if this filter is able to process a
        // particular type of object.
        // For example FilterSecurityInterceptor is able to process
        // FilterInvocation objects.
        // MethodSecurityInterceptor is able to process MethodInvocation
        // objects.
        if(!getSecureObjectClass().isAssignableFrom(object.getClass())){
        throw new IllegalArgumentException("Security invocation attempted"+
        " for object "+object.getClass().getName()+" but "+
        "AbstractSecurityInterceptor only configured to support"+
        " secure objects of type:"+getSecureObjectClass());
        }
        // Here we are retrieving the security metadata that maps to the
        // object we are receiving.
        // So if we are receiving a FilterInvocation,
        // the request is extracted from it and used to find the
        // ConfigAttribute (s) that match the request path pattern
        Collection<ConfigAttribute> attributes=this
        .obtainSecurityMetadataSource().getAttributes(object);
        if(attributes==null||attributes.isEmpty()){
        if(rejectPublicInvocations){
        throw new IllegalArgumentException("Secure object invocation "+
        ""+object+
        " was denied as public invocations are not allowed "+
        "via this interceptor. "+"This indicates a "+
        "configuration error because the "+
        "rejectPublicInvocations property is set to 'true'");
        }
        Chapter 3 ■ Spring Security Architecture and Design
        32
        if(debug){
        logger.debug("Public object - authentication not attempted");
        }
        publishEvent(new PublicInvocationEvent(object));
        return null; // no further work post-invocation
        }
        if(debug){
        logger.debug("Secure object: "+object+"; Attributes: "+
        attributes);
        }
        if(SecurityContextHolder.getContext().getAuthentication()==null){
        credentialsNotFound(messages.getMessage
        ("AbstractSecurityInterceptor.authenticationNotFound",
        "An Authentication object was not found in the "
        +"SecurityContext"),object,attributes);
        }
        Authentication authenticated=authenticateIfRequired();
        // Here we are calling the decision manager to decide if
        // authorization is granted or not.
        // This will trigger the voting mechanism,
        // and in case that access is
        // not granted an exception
        // should be thrown.
        try{
        this.accessDecisionManager.decide(authenticated,object,
        attributes);
        }catch(AccessDeniedException accessDeniedException){
        publishEvent(new AuthorizationFailureEvent(object,attributes,
        authenticated,accessDeniedException));
        throw accessDeniedException;
        }
        if(debug){
        logger.debug("Authorization successful");
        }
        if(publishAuthorizationSuccess){
        publishEvent(new AuthorizedEvent(object,attributes,
        authenticated));
        }
        // Here it will try to use the run-as functionality of Spring
        // Security that allows a user
        // to impersonate another one acquiring its security roles,
        // or more precisely, its
        // GrantedAuthority (s)
        Authentication runAs=this.runAsManager.buildRunAs(authenticated,
        object,attributes);
        Chapter 3 ■ Spring Security Architecture and Design
        33
        if(runAs==null){
        if(debug){
        logger.debug("RunAsManager did not change Authentication "+
        "object");
        }
        // no further work post-invocation
        return new InterceptorStatusToken(SecurityContextHolder
        .getContext(),false,attributes,object);
        }else{
        if(debug){
        logger.debug("Switching to RunAs Authentication: "+runAs);
        }
        SecurityContext origCtx=SecurityContextHolder.getContext();
        SecurityContextHolder.setContext(SecurityContextHolder
        .createEmptyContext());
        SecurityContextHolder.getContext().setAuthentication(runAs);
        // need to revert to token.Authenticated post-invocation
        return new InterceptorStatusToken(origCtx,true,attributes,
        object);
        }
        // If the method has not thrown an exception at this point,
        // it is safe to continue
        // the invocation through to the resource. Authorization has
        // been granted.
        }
protected Object afterInvocation(InterceptorStatusToken token,
        Object returnedObject){
        if(token==null){
        // public object
        return returnedObject;
        }
        if(token.isContextHolderRefreshRequired()){
        if(logger.isDebugEnabled()){
        logger.debug("Reverting to original Authentication: "+token
        .getSecurityContext().getAuthentication());
        }
        SecurityContextHolder.setContext(token.getSecurityContext());
        }
        // If there is an afterInvocationManager configured,
        // it will be called.
        // It will take care of filtering the return value or actually
        // throwing an exception
        // if it is relevant to do so.
        if(afterInvocationManager!=null){
        // Attempt after invocation handling
        try{
        returnedObject=afterInvocationManager.decide(token
        .getSecurityContext().getAuthentication(),
        token.getSecureObject(),token.getAttributes(),
        returnedObject);
        Chapter 3 ■ Spring Security Architecture and Design
        34
        }catch(AccessDeniedException accessDeniedException){
        AuthorizationFailureEvent event=new AuthorizationFailureEvent(
        token.getSecureObject(),token.getAttributes(),
        token.getSecurityContext().getAuthentication(),
        accessDeniedException);
        publishEvent(event);
        throw accessDeniedException;
        }
        }
        // Here is the full authorization cycled finished.
        // The response is returned to the caller.
        return returnedObject;
        }